function plot() {
    // Get input values
    const A = parseFloat(document.getElementById('val-A').value);
    const B = parseFloat(document.getElementById('val-B').value);
    const C = parseFloat(document.getElementById('val-C').value);
    const minimumX = parseFloat(document.getElementById('min-x').value);
    const maximumX = parseFloat(document.getElementById('max-x').value);

    // Error handling for invalid input values
    if (isNaN(A)) {
        alert('Please enter a valid value for A');
        return;
    }
    if (isNaN(B)) {
        alert('Please enter a valid value for B');
        return;
    }
    if (isNaN(C)) {
        alert('Please enter a valid value for C');
        return;
    }
    if (isNaN(minimumX)) {
        alert('Please enter a valid value for Minimum X');
        return;
    }
    if (isNaN(maximumX)) {
        alert('Please enter a valid value for Maximum X');
        return;
    }
    if (minimumX >= maximumX) {
        alert('Minimum X should be less than Maximum X');
        return;
    }

    // Generate x values
    const xVal = [];
    for (let x = minimumX; x <= maximumX; x += 0.1) {
        xVal.push(x);
    }

    // Generate y values based on the given function
    const yVal = xVal.map(x => {
        const k = 1;
        return Math.exp(-A * x) * Math.sin(B * k * Math.pow(x, C));
    });

    // Create a trace for the plot
    const trace = {
        x: xVal,
        y: yVal,
        mode: 'lines',
        type: 'scatter'
    };

    // Define the layout for the plot
    const layout = {
        title: 'Function Graph plot',
        xaxis: {
            title: "Value of 'x'"
        },
        yaxis: {
            title: "Value of 'y'"
        }
    };

    // Create the plot using Plotly library
    Plotly.newPlot('graph', [trace], layout);
}