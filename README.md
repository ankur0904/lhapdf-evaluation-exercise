# LHAPDF-evaluation-exercise

## Documentation of my attempt and thinking behind the project

The step by step process and the thinking behind the processes I have followed

- Created the `HTML`, `CSS` and `JS` files for the project and connected the `CSS` and `JS` files to the `HTML` file so that it will load while opening the `HTML` file.

- Included the CDN of all the required external framework in the HTML file.

- For the UI part I have used the `Bootstrap` so that it will be responsive and will look good on all the devices. If I need something to be changes I use the CSS in the `style.css` file.

- The main logical task (plotting of the mathematical equation) happens in the `JS` file. Here is the pictorial representation of the process.

    - ![pictorial representation](./image.jpeg)

    - `Index.html` will take the values of parameters from the user and pass it to the JS file `script.js` with the help of query selector.

        Here, is some code snippets:
        ```html
        <!-- HTML code -->
        <input class="form-control" id="min-x" aria-describedby="emailHelp" value="-10">
        ```

        ```javascript
        // JS code
        const minimumX = parseFloat(document.getElementById('min-x').value);
        ```

    - `script.js` will take the values of parameters.

        - I had created a function so that invoking the code become easy.
        - It'll ensure that the values are valid and the values are in int or float format.
        - Calculating the value of expression for each value of x and storing it in an array or list. The range of x is provided by the user. With these our x and f(x) values are mapped to each other.
            ```javascript
            // Relevant code
            const xVal = [];
            for (let x = minimumX; x <= maximumX; x += 0.1) {
                xVal.push(x);
            }

            const yVal = xVal.map(x => {
                const k = 1;
                return Math.exp(-A * x) * Math.sin(B * k * Math.pow(x, C));
            });
            ```
        - Configuring different variables (lables, name etc.) for the external framework in my case I am using `Plotly.js` for plotting the graph.
            ```javascript
            // Relevant code
                const trace = {
                    x: xVal,
                    y: yVal,
                    mode: 'lines',
                    type: 'scatter'
                };

                const layout = {
                    title: 'Function Graph plot',
                    xaxis: {
                        title: "Value of 'x'"
                    },
                    yaxis: {
                        title: "Value of 'y'"
                    }
                };
            ```

This is the whole intuition of the project.

References:

- https://www.jadeglobal.com/blog/6-reasons-use-bootstrap-5-better-ui-development
- https://plotly.com/javascript/getting-started/
- https://plotly.com/javascript/
- https://www.scichart.com/blog/alternatives-to-plotly-js/#bs-cookie-bar
- https://getbootstrap.com/
- https://getbootstrap.com/docs/5.3/getting-started/introduction/