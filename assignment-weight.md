### Assignment Weight

- The quality of the resulting plot is good as it provides detailed information about the (x, y) coordinates at each point of the graph. Additionally, it is movable for further exploration and can be used to generate publication plots in this way as well.

- Framework:
	- plotly.js: I have used this framework for plotting the mathematical equation for a range of 'x'
    - Why plotly.js?
        - It have features like interactive zooming, moving, and focusing on a particular part.
        - fully featured 2D and 3D chart library with plenty of chart types including line, scatter, bar, pie etc.
        - open source and community support 
        - able to support subplot
	
    - Bootstrap:  I have used this framework for the UI part. 
	- Why bootstrap?
        - cross-browser compatibility
        - avoid repetitions of code 
        - provide a reusable suit of HTML, CSS & JS
        - save development time by using predefined CSS classes

- Approach implementing a plotting backend for functions that have to be evaluated in Python code

    - First, decide on the Python backend framework I'd prefer to go with Flask because of its lightweight, simple and controllability.
    - As we need to build plotting backend for different - different functions we will pass the functions, we will pass the function as parameters along with its variables and constant value
    - Then define the two arrays or lists of the value of 'y' and 'x'
        - x = [ ... ]
        - y = f( x ) = [ ... ]
    - Then we can utilize the external plotting framework to plot the graph. I'll go with the plotly Python framework and support of different chart types and community supports.
    - And serves the plot to the front end with HTML, CSS & JS.


References:

- https://www.jadeglobal.com/blog/6-reasons-use-bootstrap-5-better-ui-development
- https://plotly.com/javascript/getting-started/
- https://plotly.com/javascript/
- https://www.scichart.com/blog/alternatives-to-plotly-js/#bs-cookie-bar
- https://getbootstrap.com/
- https://getbootstrap.com/docs/5.3/getting-started/introduction/